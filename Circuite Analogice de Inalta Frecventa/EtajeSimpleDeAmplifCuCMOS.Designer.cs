﻿namespace Circuite_Analogice_de_Inalta_Frecventa
{
    partial class EtajeSimpleDeAmplifCuCMOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabEtaje_simple_de_amplificare_cu_TMOS = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.analizeDeSemnalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.semnalMareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.semnalMicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.PictureLogo = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.btnSimulare = new System.Windows.Forms.Button();
            this.tbIdPMOS = new System.Windows.Forms.TextBox();
            this.tbWpeLNMOS = new System.Windows.Forms.TextBox();
            this.tbWpeLPMOS = new System.Windows.Forms.TextBox();
            this.tbVdsatNMOS = new System.Windows.Forms.TextBox();
            this.tbVdsatPMOS = new System.Windows.Forms.TextBox();
            this.tbIdNMOS = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.tbVdsatN = new System.Windows.Forms.TextBox();
            this.tbVdsatP = new System.Windows.Forms.TextBox();
            this.tbVthN = new System.Windows.Forms.TextBox();
            this.tbVthP = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tbWpeLP = new System.Windows.Forms.TextBox();
            this.tbWpeLN = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.tbIdrenaP = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tbIdrenN = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblVdsat = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblVSS = new System.Windows.Forms.Label();
            this.lblVDD = new System.Windows.Forms.Label();
            this.lblVin_pk = new System.Windows.Forms.Label();
            this.lblCL = new System.Windows.Forms.Label();
            this.lblBandaFrecv = new System.Windows.Forms.Label();
            this.lblCerinta = new System.Windows.Forms.Label();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.proiectareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.amplificatorSursaComunaCuSarcinaRezistivaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.amplificatorulCascodaCuSarcinaRezistivaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.amplificatorulCascodaSimetricaCuSarcinaSursaDeCurentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tabEtaje_simple_de_amplificare_cu_TMOS.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureLogo)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabEtaje_simple_de_amplificare_cu_TMOS
            // 
            this.tabEtaje_simple_de_amplificare_cu_TMOS.Controls.Add(this.tabPage1);
            this.tabEtaje_simple_de_amplificare_cu_TMOS.Controls.Add(this.tabPage2);
            this.tabEtaje_simple_de_amplificare_cu_TMOS.Controls.Add(this.tabPage3);
            this.tabEtaje_simple_de_amplificare_cu_TMOS.Controls.Add(this.tabPage4);
            this.tabEtaje_simple_de_amplificare_cu_TMOS.Location = new System.Drawing.Point(0, 0);
            this.tabEtaje_simple_de_amplificare_cu_TMOS.Name = "tabEtaje_simple_de_amplificare_cu_TMOS";
            this.tabEtaje_simple_de_amplificare_cu_TMOS.SelectedIndex = 0;
            this.tabEtaje_simple_de_amplificare_cu_TMOS.Size = new System.Drawing.Size(885, 514);
            this.tabEtaje_simple_de_amplificare_cu_TMOS.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.pictureBox11);
            this.tabPage1.Controls.Add(this.pictureBox9);
            this.tabPage1.Controls.Add(this.pictureBox8);
            this.tabPage1.Controls.Add(this.pictureBox7);
            this.tabPage1.Controls.Add(this.pictureBox6);
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.pictureBox5);
            this.tabPage1.Controls.Add(this.pictureBox4);
            this.tabPage1.Controls.Add(this.pictureBox3);
            this.tabPage1.Controls.Add(this.menuStrip1);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.pictureBox10);
            this.tabPage1.Controls.Add(this.PictureLogo);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(877, 488);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Amplificator sursa comuna cu sarcina rezistiva";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.inf_GBW;
            this.pictureBox11.Location = new System.Drawing.Point(27, 32);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(825, 329);
            this.pictureBox11.TabIndex = 11;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.prod_amplif_banda_amplif_cu_sarcina_rezistiva;
            this.pictureBox9.Location = new System.Drawing.Point(484, 380);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(241, 75);
            this.pictureBox9.TabIndex = 9;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.MouseEnter += new System.EventHandler(this.pictureBox9_MouseEnter);
            this.pictureBox9.MouseLeave += new System.EventHandler(this.pictureBox9_MouseLeave);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.poz_pol_zerouri_la_amplif_cu_sarcina_rezistiva;
            this.pictureBox8.Location = new System.Drawing.Point(559, 171);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(280, 190);
            this.pictureBox8.TabIndex = 8;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.amplif_semnal_mic_la_amplif_cu_sarcina_rezistiva;
            this.pictureBox7.Location = new System.Drawing.Point(28, 162);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(475, 73);
            this.pictureBox7.TabIndex = 7;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.analiza_de_semnal_mare_amplif_cu_sarcina_rezistiva;
            this.pictureBox6.Location = new System.Drawing.Point(124, 6);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(760, 147);
            this.pictureBox6.TabIndex = 6;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.MouseHover += new System.EventHandler(this.pictureBox6_MouseHover);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.info;
            this.pictureBox2.Location = new System.Drawing.Point(47, 69);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(71, 71);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.analiza_semnal_mare_amplif_cu_sarcina_rezistiva;
            this.pictureBox5.Location = new System.Drawing.Point(436, 260);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(330, 222);
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.Domeniul_de_variatia_a_tens_de_iesire;
            this.pictureBox4.Location = new System.Drawing.Point(9, 312);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(432, 152);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseHover += new System.EventHandler(this.pictureBox4_MouseHover);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.Determinarea_PSF;
            this.pictureBox3.Location = new System.Drawing.Point(509, 21);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(283, 201);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.analizeDeSemnalToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(9, 3);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(122, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // analizeDeSemnalToolStripMenuItem
            // 
            this.analizeDeSemnalToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.semnalMareToolStripMenuItem,
            this.semnalMicToolStripMenuItem});
            this.analizeDeSemnalToolStripMenuItem.Name = "analizeDeSemnalToolStripMenuItem";
            this.analizeDeSemnalToolStripMenuItem.Size = new System.Drawing.Size(114, 20);
            this.analizeDeSemnalToolStripMenuItem.Text = "Analize de semnal";
            // 
            // semnalMareToolStripMenuItem
            // 
            this.semnalMareToolStripMenuItem.Name = "semnalMareToolStripMenuItem";
            this.semnalMareToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.semnalMareToolStripMenuItem.Text = "Semnal Mare";
            this.semnalMareToolStripMenuItem.Click += new System.EventHandler(this.semnalMareToolStripMenuItem_Click);
            // 
            // semnalMicToolStripMenuItem
            // 
            this.semnalMicToolStripMenuItem.Name = "semnalMicToolStripMenuItem";
            this.semnalMicToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.semnalMicToolStripMenuItem.Text = "Semnal Mic";
            this.semnalMicToolStripMenuItem.Click += new System.EventHandler(this.semnalMicToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.Amplif_simplu_cu_sarcina_rezistiva;
            this.pictureBox1.Location = new System.Drawing.Point(27, 58);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(377, 248);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.Teorema_lui_miller;
            this.pictureBox10.Location = new System.Drawing.Point(61, 241);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(355, 237);
            this.pictureBox10.TabIndex = 10;
            this.pictureBox10.TabStop = false;
            // 
            // PictureLogo
            // 
            this.PictureLogo.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.rsz_vlogo;
            this.PictureLogo.Location = new System.Drawing.Point(750, 362);
            this.PictureLogo.Name = "PictureLogo";
            this.PictureLogo.Size = new System.Drawing.Size(125, 126);
            this.PictureLogo.TabIndex = 64;
            this.PictureLogo.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.pictureBox18);
            this.tabPage2.Controls.Add(this.pictureBox14);
            this.tabPage2.Controls.Add(this.pictureBox13);
            this.tabPage2.Controls.Add(this.pictureBox12);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(877, 488);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Amplificatorul cascoda cu sarcina rezistiva";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.rsz_vlogo;
            this.pictureBox18.Location = new System.Drawing.Point(750, 362);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(125, 126);
            this.pictureBox18.TabIndex = 65;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.analiza_de_semnal_mic_cascoda;
            this.pictureBox14.Location = new System.Drawing.Point(447, 6);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(419, 322);
            this.pictureBox14.TabIndex = 2;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.MouseHover += new System.EventHandler(this.pictureBox14_MouseHover);
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.Domeniul_de_variatia_a_tens_de_iesire_cascoda2;
            this.pictureBox13.Location = new System.Drawing.Point(166, 334);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(504, 142);
            this.pictureBox13.TabIndex = 1;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.MouseHover += new System.EventHandler(this.pictureBox13_MouseHover);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.schema_amplif_cascoda_cu_sarcina_rezistiv;
            this.pictureBox12.Location = new System.Drawing.Point(8, 6);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(272, 322);
            this.pictureBox12.TabIndex = 0;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.MouseHover += new System.EventHandler(this.pictureBox12_MouseHover);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.pictureBox19);
            this.tabPage3.Controls.Add(this.pictureBox17);
            this.tabPage3.Controls.Add(this.pictureBox16);
            this.tabPage3.Controls.Add(this.pictureBox15);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(877, 488);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Amplificatorul cascoda simetrica cu sarcina sursa de curent";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.rsz_vlogo;
            this.pictureBox19.Location = new System.Drawing.Point(750, 362);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(125, 126);
            this.pictureBox19.TabIndex = 65;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.analiza_semnal_mic_amplif_cascoda_simetrica;
            this.pictureBox17.Location = new System.Drawing.Point(420, 186);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(398, 270);
            this.pictureBox17.TabIndex = 2;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.psf_cascoda_simetrica;
            this.pictureBox16.Location = new System.Drawing.Point(420, 16);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(398, 164);
            this.pictureBox16.TabIndex = 1;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.schema_amplif_cascoda_simetrica;
            this.pictureBox15.Location = new System.Drawing.Point(9, 7);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(276, 412);
            this.pictureBox15.TabIndex = 0;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.MouseHover += new System.EventHandler(this.pictureBox15_MouseHover);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.pictureBox20);
            this.tabPage4.Controls.Add(this.btnSimulare);
            this.tabPage4.Controls.Add(this.tbIdPMOS);
            this.tabPage4.Controls.Add(this.tbWpeLNMOS);
            this.tabPage4.Controls.Add(this.tbWpeLPMOS);
            this.tabPage4.Controls.Add(this.tbVdsatNMOS);
            this.tabPage4.Controls.Add(this.tbVdsatPMOS);
            this.tabPage4.Controls.Add(this.tbIdNMOS);
            this.tabPage4.Controls.Add(this.label30);
            this.tabPage4.Controls.Add(this.label29);
            this.tabPage4.Controls.Add(this.label28);
            this.tabPage4.Controls.Add(this.label25);
            this.tabPage4.Controls.Add(this.label26);
            this.tabPage4.Controls.Add(this.label27);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.tbVdsatN);
            this.tabPage4.Controls.Add(this.tbVdsatP);
            this.tabPage4.Controls.Add(this.tbVthN);
            this.tabPage4.Controls.Add(this.tbVthP);
            this.tabPage4.Controls.Add(this.label23);
            this.tabPage4.Controls.Add(this.label22);
            this.tabPage4.Controls.Add(this.tbWpeLP);
            this.tabPage4.Controls.Add(this.tbWpeLN);
            this.tabPage4.Controls.Add(this.label21);
            this.tabPage4.Controls.Add(this.tbIdrenaP);
            this.tabPage4.Controls.Add(this.label20);
            this.tabPage4.Controls.Add(this.label19);
            this.tabPage4.Controls.Add(this.label18);
            this.tabPage4.Controls.Add(this.tbIdrenN);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Controls.Add(this.label3);
            this.tabPage4.Controls.Add(this.label2);
            this.tabPage4.Controls.Add(this.lblVdsat);
            this.tabPage4.Controls.Add(this.label1);
            this.tabPage4.Controls.Add(this.lblVSS);
            this.tabPage4.Controls.Add(this.lblVDD);
            this.tabPage4.Controls.Add(this.lblVin_pk);
            this.tabPage4.Controls.Add(this.lblCL);
            this.tabPage4.Controls.Add(this.lblBandaFrecv);
            this.tabPage4.Controls.Add(this.lblCerinta);
            this.tabPage4.Controls.Add(this.menuStrip2);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(877, 488);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Proiectare";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.rsz_vlogo;
            this.pictureBox20.Location = new System.Drawing.Point(750, 362);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(125, 126);
            this.pictureBox20.TabIndex = 65;
            this.pictureBox20.TabStop = false;
            // 
            // btnSimulare
            // 
            this.btnSimulare.Location = new System.Drawing.Point(21, 400);
            this.btnSimulare.Name = "btnSimulare";
            this.btnSimulare.Size = new System.Drawing.Size(75, 23);
            this.btnSimulare.TabIndex = 52;
            this.btnSimulare.Text = "Simuleaza!";
            this.btnSimulare.UseVisualStyleBackColor = true;
            this.btnSimulare.Click += new System.EventHandler(this.btnSimulare_Click);
            // 
            // tbIdPMOS
            // 
            this.tbIdPMOS.Location = new System.Drawing.Point(96, 347);
            this.tbIdPMOS.Name = "tbIdPMOS";
            this.tbIdPMOS.Size = new System.Drawing.Size(52, 20);
            this.tbIdPMOS.TabIndex = 51;
            this.tbIdPMOS.Text = "100";
            // 
            // tbWpeLNMOS
            // 
            this.tbWpeLNMOS.Location = new System.Drawing.Point(164, 321);
            this.tbWpeLNMOS.Name = "tbWpeLNMOS";
            this.tbWpeLNMOS.Size = new System.Drawing.Size(52, 20);
            this.tbWpeLNMOS.TabIndex = 50;
            this.tbWpeLNMOS.Text = "0";
            // 
            // tbWpeLPMOS
            // 
            this.tbWpeLPMOS.Location = new System.Drawing.Point(164, 347);
            this.tbWpeLPMOS.Name = "tbWpeLPMOS";
            this.tbWpeLPMOS.Size = new System.Drawing.Size(52, 20);
            this.tbWpeLPMOS.TabIndex = 49;
            this.tbWpeLPMOS.Text = "0";
            // 
            // tbVdsatNMOS
            // 
            this.tbVdsatNMOS.Location = new System.Drawing.Point(238, 321);
            this.tbVdsatNMOS.Name = "tbVdsatNMOS";
            this.tbVdsatNMOS.Size = new System.Drawing.Size(52, 20);
            this.tbVdsatNMOS.TabIndex = 48;
            this.tbVdsatNMOS.Text = "0";
            // 
            // tbVdsatPMOS
            // 
            this.tbVdsatPMOS.Location = new System.Drawing.Point(238, 347);
            this.tbVdsatPMOS.Name = "tbVdsatPMOS";
            this.tbVdsatPMOS.Size = new System.Drawing.Size(52, 20);
            this.tbVdsatPMOS.TabIndex = 47;
            this.tbVdsatPMOS.Text = "0";
            // 
            // tbIdNMOS
            // 
            this.tbIdNMOS.Location = new System.Drawing.Point(96, 321);
            this.tbIdNMOS.Name = "tbIdNMOS";
            this.tbIdNMOS.Size = new System.Drawing.Size(52, 20);
            this.tbIdNMOS.TabIndex = 46;
            this.tbIdNMOS.Text = "100";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(235, 299);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(55, 13);
            this.label30.TabIndex = 45;
            this.label30.Text = "Vdsat(mV)";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(161, 299);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(68, 13);
            this.label29.TabIndex = 44;
            this.label29.Text = "W/L(μm/μm)";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(96, 299);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(59, 13);
            this.label28.TabIndex = 43;
            this.label28.Text = "Idrena (μA)";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(35, 347);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(38, 13);
            this.label25.TabIndex = 42;
            this.label25.Text = "PMOS";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(35, 321);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(39, 13);
            this.label26.TabIndex = 41;
            this.label26.Text = "NMOS";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(18, 299);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(71, 13);
            this.label27.TabIndex = 40;
            this.label27.Text = "Tip Tranzistor";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(18, 266);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(193, 13);
            this.label24.TabIndex = 39;
            this.label24.Text = "Dimensionarea Tranzistorului MOS 018:";
            // 
            // tbVdsatN
            // 
            this.tbVdsatN.Location = new System.Drawing.Point(238, 209);
            this.tbVdsatN.Name = "tbVdsatN";
            this.tbVdsatN.Size = new System.Drawing.Size(52, 20);
            this.tbVdsatN.TabIndex = 38;
            this.tbVdsatN.Text = "0";
            // 
            // tbVdsatP
            // 
            this.tbVdsatP.Location = new System.Drawing.Point(238, 234);
            this.tbVdsatP.Name = "tbVdsatP";
            this.tbVdsatP.Size = new System.Drawing.Size(52, 20);
            this.tbVdsatP.TabIndex = 37;
            this.tbVdsatP.Text = "0";
            // 
            // tbVthN
            // 
            this.tbVthN.Location = new System.Drawing.Point(299, 209);
            this.tbVthN.Name = "tbVthN";
            this.tbVthN.Size = new System.Drawing.Size(52, 20);
            this.tbVthN.TabIndex = 36;
            this.tbVthN.Text = "0";
            // 
            // tbVthP
            // 
            this.tbVthP.Location = new System.Drawing.Point(299, 234);
            this.tbVthP.Name = "tbVthP";
            this.tbVthP.Size = new System.Drawing.Size(52, 20);
            this.tbVthP.TabIndex = 35;
            this.tbVthP.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(296, 190);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 13);
            this.label23.TabIndex = 34;
            this.label23.Text = "VTh(mV)";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(235, 190);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 33;
            this.label22.Text = "Vdsat(mV)";
            // 
            // tbWpeLP
            // 
            this.tbWpeLP.Location = new System.Drawing.Point(164, 235);
            this.tbWpeLP.Name = "tbWpeLP";
            this.tbWpeLP.Size = new System.Drawing.Size(52, 20);
            this.tbWpeLP.TabIndex = 32;
            this.tbWpeLP.Text = "30";
            // 
            // tbWpeLN
            // 
            this.tbWpeLN.Location = new System.Drawing.Point(164, 209);
            this.tbWpeLN.Name = "tbWpeLN";
            this.tbWpeLN.Size = new System.Drawing.Size(52, 20);
            this.tbWpeLN.TabIndex = 31;
            this.tbWpeLN.Text = "10";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(161, 190);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 13);
            this.label21.TabIndex = 30;
            this.label21.Text = "W/L(μm/μm)";
            // 
            // tbIdrenaP
            // 
            this.tbIdrenaP.Location = new System.Drawing.Point(96, 235);
            this.tbIdrenaP.Name = "tbIdrenaP";
            this.tbIdrenaP.Size = new System.Drawing.Size(52, 20);
            this.tbIdrenaP.TabIndex = 29;
            this.tbIdrenaP.Text = "50";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(96, 190);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 13);
            this.label20.TabIndex = 28;
            this.label20.Text = "Idrena (μA)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(29, 238);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 13);
            this.label19.TabIndex = 27;
            this.label19.Text = "P018";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(29, 212);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(33, 13);
            this.label18.TabIndex = 26;
            this.label18.Text = "N018";
            // 
            // tbIdrenN
            // 
            this.tbIdrenN.Location = new System.Drawing.Point(96, 209);
            this.tbIdrenN.Name = "tbIdrenN";
            this.tbIdrenN.Size = new System.Drawing.Size(52, 20);
            this.tbIdrenN.TabIndex = 25;
            this.tbIdrenN.Text = "50";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 190);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 13);
            this.label17.TabIndex = 24;
            this.label17.Text = "Tip Tranzistor";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 163);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(200, 13);
            this.label16.TabIndex = 23;
            this.label16.Text = "Date Initiale pentru tranzistorul MOS 018:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(384, 400);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(187, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Pasul 5: Se dimensioneaza tranzistorul";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(422, 372);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "RL = 300Ω";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(419, 345);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "RL*Id = (Vdsat*Av)/2";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(419, 317);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(226, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "Av = -gm*RL(cand RL<<rDS) = -(2IdRL)/Vdsat";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(381, 290);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Pasul 4: Calculam RL";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(609, 266);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Id = 3mA";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(419, 266);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Alegem SR = 600V/us";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(545, 238);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(194, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "SR > 2pi * BW * Vout (SR > 565.2V/us)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(419, 238);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "SR = Id/CL";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(381, 212);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(459, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Pasul 3: Alegem un SR care sa ne permita sa calculam Id si sa indeplineasca condi" +
    "tia de mai jos";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(419, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "VOUT_max=0.9V       Av=9";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(419, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Av = vout_max / Vinpk";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(419, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "vout_max = (VDD-Vdsat) / 2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(378, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Pasul 2:  Calculam VOUT_max si Av";
            // 
            // lblVdsat
            // 
            this.lblVdsat.AutoSize = true;
            this.lblVdsat.Location = new System.Drawing.Point(419, 90);
            this.lblVdsat.Name = "lblVdsat";
            this.lblVdsat.Size = new System.Drawing.Size(79, 13);
            this.lblVdsat.TabIndex = 8;
            this.lblVdsat.Text = "Vdsat = 200mV";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(378, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Pasul 1: Alegem Vdsat > Vin_pk";
            // 
            // lblVSS
            // 
            this.lblVSS.AutoSize = true;
            this.lblVSS.Location = new System.Drawing.Point(128, 118);
            this.lblVSS.Name = "lblVSS";
            this.lblVSS.Size = new System.Drawing.Size(53, 13);
            this.lblVSS.TabIndex = 6;
            this.lblVSS.Text = "VSS = 0V";
            // 
            // lblVDD
            // 
            this.lblVDD.AutoSize = true;
            this.lblVDD.Location = new System.Drawing.Point(12, 118);
            this.lblVDD.Name = "lblVDD";
            this.lblVDD.Size = new System.Drawing.Size(55, 13);
            this.lblVDD.TabIndex = 5;
            this.lblVDD.Text = "VDD = 2V";
            // 
            // lblVin_pk
            // 
            this.lblVin_pk.AutoSize = true;
            this.lblVin_pk.Location = new System.Drawing.Point(126, 95);
            this.lblVin_pk.Name = "lblVin_pk";
            this.lblVin_pk.Size = new System.Drawing.Size(85, 13);
            this.lblVin_pk.TabIndex = 4;
            this.lblVin_pk.Text = "Vin_pk = 100mV";
            // 
            // lblCL
            // 
            this.lblCL.AutoSize = true;
            this.lblCL.Location = new System.Drawing.Point(12, 95);
            this.lblCL.Name = "lblCL";
            this.lblCL.Size = new System.Drawing.Size(50, 13);
            this.lblCL.TabIndex = 3;
            this.lblCL.Text = "CL = 5pF";
            // 
            // lblBandaFrecv
            // 
            this.lblBandaFrecv.AutoSize = true;
            this.lblBandaFrecv.Location = new System.Drawing.Point(12, 70);
            this.lblBandaFrecv.Name = "lblBandaFrecv";
            this.lblBandaFrecv.Size = new System.Drawing.Size(180, 13);
            this.lblBandaFrecv.TabIndex = 2;
            this.lblBandaFrecv.Text = "BW frecventa primului pol = 100MHz";
            // 
            // lblCerinta
            // 
            this.lblCerinta.AutoSize = true;
            this.lblCerinta.Location = new System.Drawing.Point(3, 44);
            this.lblCerinta.Name = "lblCerinta";
            this.lblCerinta.Size = new System.Drawing.Size(405, 13);
            this.lblCerinta.TabIndex = 1;
            this.lblCerinta.Text = "Se proiecteaza amplificatorul cu sarcina rezistiva tinand cont de specificatiile " +
    "impuse:";
            // 
            // menuStrip2
            // 
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.proiectareToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(9, 7);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(172, 24);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // proiectareToolStripMenuItem
            // 
            this.proiectareToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.amplificatorSursaComunaCuSarcinaRezistivaToolStripMenuItem,
            this.amplificatorulCascodaCuSarcinaRezistivaToolStripMenuItem,
            this.amplificatorulCascodaSimetricaCuSarcinaSursaDeCurentToolStripMenuItem});
            this.proiectareToolStripMenuItem.Name = "proiectareToolStripMenuItem";
            this.proiectareToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.proiectareToolStripMenuItem.Text = "Proiectare";
            // 
            // amplificatorSursaComunaCuSarcinaRezistivaToolStripMenuItem
            // 
            this.amplificatorSursaComunaCuSarcinaRezistivaToolStripMenuItem.Name = "amplificatorSursaComunaCuSarcinaRezistivaToolStripMenuItem";
            this.amplificatorSursaComunaCuSarcinaRezistivaToolStripMenuItem.Size = new System.Drawing.Size(386, 22);
            this.amplificatorSursaComunaCuSarcinaRezistivaToolStripMenuItem.Text = "Amplificator sursa comuna cu sarcina rezistiva";
            this.amplificatorSursaComunaCuSarcinaRezistivaToolStripMenuItem.Click += new System.EventHandler(this.amplificatorSursaComunaCuSarcinaRezistivaToolStripMenuItem_Click);
            // 
            // amplificatorulCascodaCuSarcinaRezistivaToolStripMenuItem
            // 
            this.amplificatorulCascodaCuSarcinaRezistivaToolStripMenuItem.Name = "amplificatorulCascodaCuSarcinaRezistivaToolStripMenuItem";
            this.amplificatorulCascodaCuSarcinaRezistivaToolStripMenuItem.Size = new System.Drawing.Size(386, 22);
            this.amplificatorulCascodaCuSarcinaRezistivaToolStripMenuItem.Text = "Amplificatorul cascoda cu sarcina rezistiva";
            this.amplificatorulCascodaCuSarcinaRezistivaToolStripMenuItem.Click += new System.EventHandler(this.amplificatorulCascodaCuSarcinaRezistivaToolStripMenuItem_Click);
            // 
            // amplificatorulCascodaSimetricaCuSarcinaSursaDeCurentToolStripMenuItem
            // 
            this.amplificatorulCascodaSimetricaCuSarcinaSursaDeCurentToolStripMenuItem.Name = "amplificatorulCascodaSimetricaCuSarcinaSursaDeCurentToolStripMenuItem";
            this.amplificatorulCascodaSimetricaCuSarcinaSursaDeCurentToolStripMenuItem.Size = new System.Drawing.Size(386, 22);
            this.amplificatorulCascodaSimetricaCuSarcinaSursaDeCurentToolStripMenuItem.Text = "Amplificatorul cascoda simetrica cu sarcina sursa de curent";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // EtajeSimpleDeAmplifCuCMOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(882, 512);
            this.Controls.Add(this.tabEtaje_simple_de_amplificare_cu_TMOS);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "EtajeSimpleDeAmplifCuCMOS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Etaje simple de amplificare cu TMOS";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EtajeSimpleDeAmplifCuCMOS_FormClosed);
            this.tabEtaje_simple_de_amplificare_cu_TMOS.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureLogo)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabEtaje_simple_de_amplificare_cu_TMOS;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem analizeDeSemnalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem semnalMareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem semnalMicToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem proiectareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem amplificatorSursaComunaCuSarcinaRezistivaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem amplificatorulCascodaCuSarcinaRezistivaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem amplificatorulCascodaSimetricaCuSarcinaSursaDeCurentToolStripMenuItem;
        private System.Windows.Forms.Label lblVSS;
        private System.Windows.Forms.Label lblVDD;
        private System.Windows.Forms.Label lblVin_pk;
        private System.Windows.Forms.Label lblCL;
        private System.Windows.Forms.Label lblBandaFrecv;
        private System.Windows.Forms.Label lblCerinta;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblVdsat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbIdrenN;
        private System.Windows.Forms.TextBox tbIdrenaP;
        private System.Windows.Forms.Button btnSimulare;
        private System.Windows.Forms.TextBox tbIdPMOS;
        private System.Windows.Forms.TextBox tbWpeLNMOS;
        private System.Windows.Forms.TextBox tbWpeLPMOS;
        private System.Windows.Forms.TextBox tbVdsatNMOS;
        private System.Windows.Forms.TextBox tbVdsatPMOS;
        private System.Windows.Forms.TextBox tbIdNMOS;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox tbVdsatN;
        private System.Windows.Forms.TextBox tbVdsatP;
        private System.Windows.Forms.TextBox tbVthN;
        private System.Windows.Forms.TextBox tbVthP;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox tbWpeLP;
        private System.Windows.Forms.TextBox tbWpeLN;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.PictureBox PictureLogo;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
    }
}