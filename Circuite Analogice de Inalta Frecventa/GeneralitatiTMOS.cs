﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Circuite_Analogice_de_Inalta_Frecventa
{
    public partial class GeneralitatiTMOS : Form
    {
        public GeneralitatiTMOS()
        {
            InitializeComponent();
            HideRegim();
            HideCaracteristici();
            HideModelare();
            pictureBox20.Hide();
            this.MaximizeBox = false;
            this.MaximumSize = this.MinimumSize = this.Size;
        }

        private void GeneralitatiTMOS_FormClosed(object sender, FormClosedEventArgs e)
        {
            mainForm mfrm = new mainForm();
            this.Visible = false;
            mfrm.ShowDialog();
            this.Close();
        }

        private void D_MouseHover(object sender, EventArgs e)
        {
            ToolTip TTD = new ToolTip();
            TTD.SetToolTip(D, "Drena");
        }

        private void G_MouseHover(object sender, EventArgs e)
        {
            ToolTip TTG = new ToolTip();
            TTG.SetToolTip(G, "Grila");
        }

        private void S_MouseHover(object sender, EventArgs e)
        {
            ToolTip TTS = new ToolTip();
            TTS.SetToolTip(S, "Sursa");
        }

        private void Sursa_MouseHover(object sender, EventArgs e)
        {
            ToolTip TTS = new ToolTip();
            TTS.SetToolTip(Sursa, "Sursa");
        }

        private void Grila_MouseHover(object sender, EventArgs e)
        {
            ToolTip TTG = new ToolTip();
            TTG.SetToolTip(Grila, "Grila");
        }

        private void Drena_MouseHover(object sender, EventArgs e)
        {
            ToolTip TTD = new ToolTip();
            TTD.SetToolTip(Drena, "Drena");
        }

        private void B_MouseHover(object sender, EventArgs e)
        {
            ToolTip TTB = new ToolTip();
            TTB.SetToolTip(B, "Bulk");
        }

        private void Bulck_MouseHover(object sender, EventArgs e)
        {
            ToolTip TTB = new ToolTip();
            TTB.SetToolTip(Bulck, "Bulk");
        }

        private void HideRegim()
        {
            // Regim Blocat
            CondB.Hide();
            pictureBox1.Hide();
            FunctionareB.Hide();
            pictureBox2.Hide();
            // Regim Sub Prag
            pictureBox3.Hide();
            pictureBox4.Hide();
            // Regim Saturatie
            pictureBox5.Hide();
            pictureBox6.Hide();
            // Regim Liniar
            pictureBox7.Hide();
            pictureBox8.Hide();
        }

        private void HideCaracteristici()
        {
            // Caracteristici Iesire/Transfer
            pictureBox9.Hide();
            pictureBox10.Hide();
        }

        private void HideModelare()
        {
            pictureBox11.Hide();
            pictureBox12.Hide();
            pictureBox14.Hide();
            pictureBox15.Hide();
        }

        private void regimBlocatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideRegim();
            CondB.Show();
            FunctionareB.Show();
            pictureBox1.Show();
            pictureBox2.Show();
        }

        private void regimSubpragToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideRegim();
            CondB.Show();
            FunctionareB.Show();
            pictureBox3.Show();
            pictureBox4.Show();
        }

        private void regimSaturatieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideRegim();
            CondB.Show();
            FunctionareB.Show();
            pictureBox5.Show();
            pictureBox6.Show();
        }

        private void regimLiniarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideRegim();
            CondB.Show();
            FunctionareB.Show();
            pictureBox7.Show();
            pictureBox8.Show();
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideRegim();
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            ToolTip CTB = new ToolTip();
            CTB.IsBalloon = true;
            CTB.AutoPopDelay = 30000;
            CTB.SetToolTip(pictureBox1, "Tensiunea Grila-Sursa mult mai mica decat Tensiunea de prag");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            ToolTip FTB = new ToolTip();
            FTB.IsBalloon = true;
            FTB.AutoPopDelay = 30000;
            FTB.SetToolTip(pictureBox2, "Curentul de drena = 0\nDe fapt o valoare foarte mica -> curent de pierderi (leakage current)\nAplicatii: Se foloseste ca dispozitiv cu functie de comutare (switch in stare blocat)");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            ToolTip CTSP = new ToolTip();
            CTSP.IsBalloon = true;
            CTSP.AutoPopDelay = 30000;
            CTSP.SetToolTip(pictureBox3, "Tensiunea Grila-Sursa aproximativ egala cu Tensiunea de prag\n(Vgs in jurul valorii lui Vth)");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            ToolTip FTSP = new ToolTip();
            FTSP.IsBalloon = true;
            FTSP.AutoPopDelay = 30000;
            FTSP.SetToolTip(pictureBox4, "Curentul de drena ii exponential cu Tensiunea Grila Sursa\n(Functioneaza ca o sursa de curent comandata in tensiune)\nAplicatii:Se foloseste in circuite transliniare.");
        }
        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            ToolTip CTS = new ToolTip();
            CTS.IsBalloon = true;
            CTS.AutoPopDelay = 30000;
            CTS.SetToolTip(this.pictureBox5, "Tensiunea Grila-Sursa > Tensiunea de prag\nTensiunea Drena-Sursa > Tensiunea Drena Saturata\n Se mai poate scrie ca fiind Diferenta intre Tensiunea Grila-Sursa si Tensiunea de prag");
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            ToolTip FTS = new ToolTip();
            FTS.IsBalloon = true;
            FTS.AutoPopDelay = 30000;
            FTS.SetToolTip(pictureBox6, "μ reprezinta mobilitatea purtatorilor de sarcina, ale carui valori depind de tipul de purtatori \ndeci de tipul canalului de conductie (transistor N-MOS sau P-MOS) - si de caracteristicile\nprocesului de fabricatie; de exemplu pentru un proces CMOS standard de 0.35um avem:\nuP=250cm2/Vs;\t uN=600cm2/Vs),\nCox-reprezinta capacitatea dintre grila si canal, raportata la unitatea de arie;\nW,L dimensiunile (geometrice ale canalului de conductie) al tranzistorului.\nVth –reprezinta tensiunea de prag si are urmatoarea lege in functie de tensiunea bulk-sursa:\nVth=Vth0+γ*[radical(|2ΦF+VBS|) - radical(|2ΦF|)]\n|2ΦF|=0.6V si γ=0.5V...0.8V\nDeci pe masura ce tensiunea substrat(bulk)-sursa creste aceasta duce la marirea tensiunii de prag;\nAplicatii: Tranzistorul in regim saturat este foarte utilizat ca dispozitiv activ pentru conversia tensiunii\n(grila-sursa) in curent (de drena), element de baza in aplicatiile liniare de amplificare, filtrare, etc.");
        }

        private void pictureBox10_MouseHover(object sender, EventArgs e)
        {

            ToolTip CT = new ToolTip();
            CT.IsBalloon = true;
            CT.AutoPopDelay = 30000;
            CT.SetToolTip(pictureBox10, "Caracteristica de transfer-arata o dependenta intre semnalul de iesire (curentul Ids)\n si un semnal de intrare-comanda (tensiunea VGS).");
        }

        private void pictureBox9_MouseHover(object sender, EventArgs e)
        {
            ToolTip COUT = new ToolTip();
            COUT.IsBalloon = true;
            COUT.AutoPopDelay = 30000;
            COUT.SetToolTip(pictureBox9, "Caracteristica de iesire-arata o dependenta a curentului Id in functie de tensiunea\ndrena-sursa. Se disting astfel doua regimuri de functionare-unul unde curentul creste\nproportional cu tensiunea VDS si unul in care acesta devine saturat fiind invariabil la\ncresterea tensiunii VDS. In aceasta figura sunt prezentate aceste aspecte dar si variatia\ncurentului in functie de tensiunea VGS(familie de caracteristici).");
        }

        private void pictureBoxSimbol_MouseHover(object sender, EventArgs e)
        {
            ToolTip CMOSFUN = new ToolTip();
            CMOSFUN.IsBalloon = true;
            CMOSFUN.AutoPopDelay = 30000;
            CMOSFUN.SetToolTip(pictureBoxSimbol, "Principiul de funcţionare al acestor dispozitive este utilizarea unei tensiuni între două din\nterminalele de comandă; pentru a controla intensitatea curentului prin cel de-al treilea terminal \n(terminalul de execuţie). Astfel, in functie de valorile anumitor parametri specifici acestor tipuri de dispozitive active,\nse pot determina doua tipuri de caracteristici ce ilustreaza interdependenta parametrilor de semnal mic ai\ntranzistoului MOS cu cea a semnalelor electrice (tensiuni,curenti) ce sunt aplicate sau culese.");
        }

        private void caracteristicaDeTransferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideCaracteristici();
            pictureBox10.Show();
        }

        private void caracteristicaDeIesireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideCaracteristici();
            pictureBox9.Show();
        }

        private void clearToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            HideCaracteristici();
        }

        private void pictureBox8_MouseHover(object sender, EventArgs e)
        {
            ToolTip CTL = new ToolTip();
            CTL.IsBalloon = true;
            CTL.AutoPopDelay = 30000;
            CTL.SetToolTip(this.pictureBox8, "μ reprezinta mobilitatea purtatorilor de sarcina, ale carui valori depind de tipul de purtatori \ndeci de tipul canalului de conductie (transistor N-MOS sau P-MOS) - si de caracteristicile\nprocesului de fabricatie; de exemplu pentru un proces CMOS standard de 0.35um avem:\nuP=250cm2/Vs;\t uN=600cm2/Vs),\niD-curentul de drena\nVDS-Tensiunea Drena-sursa\nCox-reprezinta capacitatea dintre grila si canal, raportata la unitatea de arie;\nW,L dimensiunile (geometrice ale canalului de conductie) al tranzistorului.\nVth –reprezinta tensiunea de prag\nAplicatia principala a dispozitivului activ in regim liniar este aceea de rezistenta\ncomandata in tensiune.");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            ToolTip CTL = new ToolTip();
            CTL.IsBalloon = true;
            CTL.AutoPopDelay = 30000;
            CTL.SetToolTip(this.pictureBox7, "Tensiunea Grila-Sursa > Tensiunea de prag\nTensiunea Drena-Sursa < Tensiunea Drena Saturata\n Se mai poate scrie ca fiind Diferenta intre Tensiunea Grila-Sursa si Tensiunea de prag");
        }

        private void pictureBox11_MouseHover(object sender, EventArgs e)
        {
            ToolTip ModelSemnalMicSimplificat = new ToolTip();
            ModelSemnalMicSimplificat.IsBalloon = true;
            ModelSemnalMicSimplificat.AutoPopDelay = 30000;
            ModelSemnalMicSimplificat.SetToolTip(this.pictureBox11, "Modelul simplificat de semnal mic la joasa frecventa al unui tranzistor aflat in regim saturat.\nAceast model nu reprezinta efectul tensiunii dintre sursa si substrat (bulk) – deci va\nfi valabil numai pentru cazul in care substratul (bulk) este conectat la sursa, VBS=0. De asemenea, nu sunt\nmodelate capacitatile parazite si unle efecte de ordinul doi...");
        }

        private void modelSimplificatDeSemnalMicJoasaFrecventaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideModelare();
            pictureBox11.Show();
            pictureBox12.Show();
            pictureBox15.Show();
        }

        private void pictureBox12_MouseHover(object sender, EventArgs e)
        {
            ToolTip Early = new ToolTip();
            Early.IsBalloon = true;
            Early.AutoPopDelay = 30000;
            Early.SetToolTip(this.pictureBox12, "Unde L este lungimea grilei. Prin analogie cu modelul tranzistorului bipolar parametrul VE este\nuneori numit „tensiune Early”, desi dupa cum reiese si din formula unitatea sa de masura este: V/um.");
        }

        private void modelDeSemnalMicSiInaltaFrecventaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideModelare();
            pictureBox12.Show();
            pictureBox14.Show();
            pictureBox15.Show();
        }

        private void pictureBox14_MouseHover(object sender, EventArgs e)
        {
            ToolTip SSMJF = new ToolTip();
            SSMJF.IsBalloon = true;
            SSMJF.AutoPopDelay = 30000;
            SSMJF.SetToolTip(this.pictureBox14, "Se observa ca pe langa parametri de semnal mic prezentati anterior, la frecvente inalte mai apar si\no serie de elemente parazite precum capacitatile dintre cele patru terminale, cat si rezistenta grilei.\nUn alt dezavantaj major al acestui dispozitiv il reprezinta transconductanta gmVBS ce apare\ndatorita diferentei de potential nenule intre substrat si sursa. Acest neajuns apare datorita faptului ca nu in\ntoate tehnologiile CMOS se poate implementa fizic ca terminalul substratului sa fie mereu conectat la\nterminalul sursei pentru fiecare tranzistor. De exemplu in tehnologia CMOS cu subtrat de tip P, toate\ntranzistoarele de tip NMOS au substratul conectat la acelasi potential si anume VSS. Acest neajuns este\ndemonstrat si practic la verificarea experimentala, unde se arata prin simulari, ca transconductanta totala a\ntranzistorului scade odata cu VBS=/=0, si ca valoarea tensiunii de prag de unde acesta incepe sa conduca\ncreste.");
        }

        private void pictureBox15_MouseHover(object sender, EventArgs e)
        {
            ToolTip Grafic = new ToolTip();
            Grafic.IsBalloon = true;
            Grafic.AutoPopDelay = 30000;
            Grafic.SetToolTip(this.pictureBox15, "In primul rind se observa impartirea regiunii de saturatie in doua sub-regiuni: inversiune\nputernica (strong inversion, si), in care transconducatnta gm este (aprox) proportional cu vdsat,\n(cu valori relativ mici tipic 400mV); pentru valori mai mari ale lui vdsat\ntransconductanta gm ramine aprox constanta, datorita efectului de saturare a vitezei purtatorilor de\nsarcina.In conditiile in care pentru acelasi curent Ids, gm poate varia exponential cu vGS(sub-prag), liniar cu\nvdsat (saturatie) sau ramine constant la variatia lui Vgs(vdsat)se pune problema gasirii punctului de\nfunctionare care maximizeaza valoarea lui gm in raport cu curentul de polarizare. De obicei acest punct\nstatic optim dpdv al raportului gm/Ids se gaseste la granita dintre regiunile de inversie slaba si puternica\n(weak & strong inversion). wi & si");
        }

        private void btnSimulation_Click(object sender, EventArgs e)
        {
            SimuleazaPSF();
        }
        private void SimuleazaPSF()
        {
            float gm, rds, gds, Vdsat, W, L, Id, μCox;
            if(tbW != null && tbL != null && tbId != null)
            {
                Id = float.Parse(tbId.Text);
                W = float.Parse(tbW.Text);
                L = float.Parse(tbL.Text);

                Vdsat = float.Parse(tbVdsat.Text);
                gm = (2 * Id) / Vdsat;
                tbgm.Text = gm.ToString(); 
            }            
        }

        private void pictureBox18_MouseHover(object sender, EventArgs e)
        {
            ToolTip Info = new ToolTip();
            Info.IsBalloon = true;
            Info.AutoPopDelay = 30000;
            Info.SetToolTip(this.pictureBox18, "In cazul dimensionarii tranzistorilor MOS avem la dispozitie patru parametri care sunt utilizati in\ndoua ecuatii. Astfel obtinem doua grade de libertate ce ne permit alegerea a doua din cele patru variabile \nsi dimensionarea corespunzatoare a celorlalte  utilizand sistemul rezultat cu doua ecuatii.");
        }

        private void pictureBox19_MouseHover(object sender, EventArgs e)
        {
            ToolTip Grad = new ToolTip();
            Grad.IsBalloon = true;
            Grad.AutoPopDelay = 30000;
            Grad.SetToolTip(this.pictureBox19, "Parametrul uCox nu este recomandat ca parametru de dimensionare deoarece valoare acestuia\nvariaza odata cu procesul tehnologic si temperatura. Pentru a elimina acest neajuns se foloseste un set de\nreferinta pentru cele patru variabile determinat prin simulari, si prin raportarea valorilor parametrilor\nnecesari in circuitul de dimensionat, la aceste valori de referinta se reduce parametrul  uCox ca in sistemul\nde mai sus.");
            pictureBox20.Show();
            pictureBox16.Hide();
            pictureBox17.Hide();
            pictureBox18.Hide();
        }

        private void pictureBox19_MouseLeave(object sender, EventArgs e)
        {
            pictureBox20.Hide();
            pictureBox16.Show();
            pictureBox17.Show();
            pictureBox18.Show();
        }
    }
}

// hide ModelSemnalMicSimplificat
/*
 * 
 * //float Rds;
            gm = 0;

            if(tbgm != null)
                gm = float.Parse(tbgm.Text);
            /*
            if (tbrDS != null)
                Rds = float.Parse(tbrDS.Text);
            else
                Rds = 0;

            if (tbgds != null)
                gds = float.Parse(tbgds.Text);
            else
                gds = 0;

            if (tbVdsat != null)
                Vdsat = float.Parse(tbVdsat.Text);
            else
                Vdsat = 0;
            if (tbW != null)
                W = float.Parse(tbW.Text);
            else
                W = 0;

            if (tbL != null)
                L = float.Parse(tbL.Text);
            else
                L = 0;

            if (tbId != null)
                Id = float.Parse(tbId.Text);
            else
                Id = 0;

            μCox = 0;
            gm = (2 * Id) / Vdsat;
            μCox = (L * gm) / W;
 * 
ToolTip buttonToolTip = new ToolTip();
buttonToolTip.ToolTipTitle = "Button Tooltip";
buttonToolTip.UseFading = true;
buttonToolTip.UseAnimation = true;
buttonToolTip.IsBalloon = true;
 
buttonToolTip.ShowAlways = true;
 
buttonToolTip.AutoPopDelay = 5000;
buttonToolTip.InitialDelay = 1000;
buttonToolTip.ReshowDelay = 500;
 
buttonToolTip.SetToolTip(button1, "Click me to execute.");
 * 
α β γ δ ε ζ η θ ι κ λ μ ν ξ ο π ρ σ τ υ φ χ ψ ω
Α Β Γ Δ Ε Ζ Η Θ Ι Κ Λ Μ Ν Ξ Ο Π Ρ Σ Τ Υ Φ Χ Ψ Ω
 */