﻿namespace Circuite_Analogice_de_Inalta_Frecventa
{
    partial class GeneralitatiTMOS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlFunctionare = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Bulck = new System.Windows.Forms.Label();
            this.G = new System.Windows.Forms.Label();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.caracteristiciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.caracteristicaDeTransferToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.caracteristicaDeIesireToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Simbol = new System.Windows.Forms.Label();
            this.Drena = new System.Windows.Forms.Label();
            this.D = new System.Windows.Forms.Label();
            this.Sursa = new System.Windows.Forms.Label();
            this.Grila = new System.Windows.Forms.Label();
            this.FunctionareB = new System.Windows.Forms.Label();
            this.S = new System.Windows.Forms.Label();
            this.B = new System.Windows.Forms.Label();
            this.CondB = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.regimuriDeFunctionareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regimBlocatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regimSubpragToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regimSaturatieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.regimLiniarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.modeleDeSemnalMicToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modelSimplificatDeSemnalMicJoasaFrecventaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modelDeSemnalMicSiInaltaFrecventaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSimulation = new System.Windows.Forms.Button();
            this.tbrDS = new System.Windows.Forms.TextBox();
            this.lblUMrDS = new System.Windows.Forms.Label();
            this.lblRds = new System.Windows.Forms.Label();
            this.lblUMgds = new System.Windows.Forms.Label();
            this.lblUMVdsat = new System.Windows.Forms.Label();
            this.lblgmUM = new System.Windows.Forms.Label();
            this.tbgm = new System.Windows.Forms.TextBox();
            this.tbVdsat = new System.Windows.Forms.TextBox();
            this.tbgds = new System.Windows.Forms.TextBox();
            this.lblgds = new System.Windows.Forms.Label();
            this.lblVdsat = new System.Windows.Forms.Label();
            this.lblgm = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblObiectiv = new System.Windows.Forms.Label();
            this.lblIdUM = new System.Windows.Forms.Label();
            this.lblLUM = new System.Windows.Forms.Label();
            this.lblWUM = new System.Windows.Forms.Label();
            this.tbId = new System.Windows.Forms.TextBox();
            this.tbL = new System.Windows.Forms.TextBox();
            this.tbW = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.lblL = new System.Windows.Forms.Label();
            this.lblW = new System.Windows.Forms.Label();
            this.PictureLogo = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxSimbol = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.tabControlFunctionare.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.menuStrip3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSimbol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlFunctionare
            // 
            this.tabControlFunctionare.Controls.Add(this.tabPage1);
            this.tabControlFunctionare.Controls.Add(this.tabPage2);
            this.tabControlFunctionare.Controls.Add(this.tabPage3);
            this.tabControlFunctionare.Location = new System.Drawing.Point(0, 1);
            this.tabControlFunctionare.Name = "tabControlFunctionare";
            this.tabControlFunctionare.SelectedIndex = 0;
            this.tabControlFunctionare.Size = new System.Drawing.Size(883, 514);
            this.tabControlFunctionare.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.PictureLogo);
            this.tabPage1.Controls.Add(this.Bulck);
            this.tabPage1.Controls.Add(this.pictureBox10);
            this.tabPage1.Controls.Add(this.pictureBox9);
            this.tabPage1.Controls.Add(this.G);
            this.tabPage1.Controls.Add(this.menuStrip2);
            this.tabPage1.Controls.Add(this.Simbol);
            this.tabPage1.Controls.Add(this.pictureBox8);
            this.tabPage1.Controls.Add(this.Drena);
            this.tabPage1.Controls.Add(this.pictureBox7);
            this.tabPage1.Controls.Add(this.D);
            this.tabPage1.Controls.Add(this.pictureBox6);
            this.tabPage1.Controls.Add(this.pictureBox5);
            this.tabPage1.Controls.Add(this.Sursa);
            this.tabPage1.Controls.Add(this.pictureBox4);
            this.tabPage1.Controls.Add(this.Grila);
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.FunctionareB);
            this.tabPage1.Controls.Add(this.pictureBox3);
            this.tabPage1.Controls.Add(this.S);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.B);
            this.tabPage1.Controls.Add(this.pictureBoxSimbol);
            this.tabPage1.Controls.Add(this.CondB);
            this.tabPage1.Controls.Add(this.menuStrip1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(875, 488);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Functionare";
            // 
            // Bulck
            // 
            this.Bulck.AutoSize = true;
            this.Bulck.Location = new System.Drawing.Point(247, 67);
            this.Bulck.Name = "Bulck";
            this.Bulck.Size = new System.Drawing.Size(14, 13);
            this.Bulck.TabIndex = 47;
            this.Bulck.Text = "B";
            this.Bulck.MouseHover += new System.EventHandler(this.Bulck_MouseHover);
            // 
            // G
            // 
            this.G.AutoSize = true;
            this.G.Location = new System.Drawing.Point(-1, 67);
            this.G.Name = "G";
            this.G.Size = new System.Drawing.Size(15, 13);
            this.G.TabIndex = 41;
            this.G.Text = "G";
            this.G.MouseHover += new System.EventHandler(this.G_MouseHover);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.caracteristiciToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(500, 2);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(95, 24);
            this.menuStrip2.TabIndex = 62;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // caracteristiciToolStripMenuItem
            // 
            this.caracteristiciToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.caracteristicaDeTransferToolStripMenuItem,
            this.caracteristicaDeIesireToolStripMenuItem});
            this.caracteristiciToolStripMenuItem.Name = "caracteristiciToolStripMenuItem";
            this.caracteristiciToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.caracteristiciToolStripMenuItem.Text = "Caracteristici";
            // 
            // caracteristicaDeTransferToolStripMenuItem
            // 
            this.caracteristicaDeTransferToolStripMenuItem.Name = "caracteristicaDeTransferToolStripMenuItem";
            this.caracteristicaDeTransferToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.caracteristicaDeTransferToolStripMenuItem.Text = "Caracteristica de transfer";
            this.caracteristicaDeTransferToolStripMenuItem.Click += new System.EventHandler(this.caracteristicaDeTransferToolStripMenuItem_Click);
            // 
            // caracteristicaDeIesireToolStripMenuItem
            // 
            this.caracteristicaDeIesireToolStripMenuItem.Name = "caracteristicaDeIesireToolStripMenuItem";
            this.caracteristicaDeIesireToolStripMenuItem.Size = new System.Drawing.Size(204, 22);
            this.caracteristicaDeIesireToolStripMenuItem.Text = "Caracteristica de iesire ";
            this.caracteristicaDeIesireToolStripMenuItem.Click += new System.EventHandler(this.caracteristicaDeIesireToolStripMenuItem_Click);
            // 
            // Simbol
            // 
            this.Simbol.AutoSize = true;
            this.Simbol.Location = new System.Drawing.Point(106, 116);
            this.Simbol.Name = "Simbol";
            this.Simbol.Size = new System.Drawing.Size(72, 13);
            this.Simbol.TabIndex = 39;
            this.Simbol.Text = "Simbol TMOS";
            // 
            // Drena
            // 
            this.Drena.AutoSize = true;
            this.Drena.Location = new System.Drawing.Point(229, 127);
            this.Drena.Name = "Drena";
            this.Drena.Size = new System.Drawing.Size(15, 13);
            this.Drena.TabIndex = 48;
            this.Drena.Text = "D";
            this.Drena.MouseHover += new System.EventHandler(this.Drena_MouseHover);
            // 
            // D
            // 
            this.D.AutoSize = true;
            this.D.Location = new System.Drawing.Point(85, 5);
            this.D.Name = "D";
            this.D.Size = new System.Drawing.Size(15, 13);
            this.D.TabIndex = 42;
            this.D.Text = "D";
            this.D.MouseHover += new System.EventHandler(this.D_MouseHover);
            // 
            // Sursa
            // 
            this.Sursa.AutoSize = true;
            this.Sursa.Location = new System.Drawing.Point(229, 2);
            this.Sursa.Name = "Sursa";
            this.Sursa.Size = new System.Drawing.Size(14, 13);
            this.Sursa.TabIndex = 46;
            this.Sursa.Text = "S";
            this.Sursa.MouseHover += new System.EventHandler(this.Sursa_MouseHover);
            // 
            // Grila
            // 
            this.Grila.AutoSize = true;
            this.Grila.Location = new System.Drawing.Point(143, 67);
            this.Grila.Name = "Grila";
            this.Grila.Size = new System.Drawing.Size(15, 13);
            this.Grila.TabIndex = 45;
            this.Grila.Text = "G";
            this.Grila.MouseHover += new System.EventHandler(this.Grila_MouseHover);
            // 
            // FunctionareB
            // 
            this.FunctionareB.AutoSize = true;
            this.FunctionareB.Location = new System.Drawing.Point(3, 282);
            this.FunctionareB.Name = "FunctionareB";
            this.FunctionareB.Size = new System.Drawing.Size(66, 13);
            this.FunctionareB.TabIndex = 50;
            this.FunctionareB.Text = "Functionare:";
            // 
            // S
            // 
            this.S.AutoSize = true;
            this.S.Location = new System.Drawing.Point(86, 127);
            this.S.Name = "S";
            this.S.Size = new System.Drawing.Size(14, 13);
            this.S.TabIndex = 43;
            this.S.Text = "S";
            this.S.MouseHover += new System.EventHandler(this.S_MouseHover);
            // 
            // B
            // 
            this.B.AutoSize = true;
            this.B.Location = new System.Drawing.Point(106, 67);
            this.B.Name = "B";
            this.B.Size = new System.Drawing.Size(14, 13);
            this.B.TabIndex = 44;
            this.B.Text = "B";
            this.B.MouseHover += new System.EventHandler(this.B_MouseHover);
            // 
            // CondB
            // 
            this.CondB.AutoSize = true;
            this.CondB.Location = new System.Drawing.Point(3, 177);
            this.CondB.Name = "CondB";
            this.CondB.Size = new System.Drawing.Size(48, 13);
            this.CondB.TabIndex = 49;
            this.CondB.Text = "Conditie:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regimuriDeFunctionareToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(-3, 153);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(155, 24);
            this.menuStrip1.TabIndex = 57;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // regimuriDeFunctionareToolStripMenuItem
            // 
            this.regimuriDeFunctionareToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.regimBlocatToolStripMenuItem,
            this.regimSubpragToolStripMenuItem,
            this.regimSaturatieToolStripMenuItem,
            this.regimLiniarToolStripMenuItem});
            this.regimuriDeFunctionareToolStripMenuItem.Name = "regimuriDeFunctionareToolStripMenuItem";
            this.regimuriDeFunctionareToolStripMenuItem.Size = new System.Drawing.Size(147, 20);
            this.regimuriDeFunctionareToolStripMenuItem.Text = "Regimuri de functionare";
            // 
            // regimBlocatToolStripMenuItem
            // 
            this.regimBlocatToolStripMenuItem.Name = "regimBlocatToolStripMenuItem";
            this.regimBlocatToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.regimBlocatToolStripMenuItem.Text = "Regim Blocat";
            this.regimBlocatToolStripMenuItem.Click += new System.EventHandler(this.regimBlocatToolStripMenuItem_Click);
            // 
            // regimSubpragToolStripMenuItem
            // 
            this.regimSubpragToolStripMenuItem.Name = "regimSubpragToolStripMenuItem";
            this.regimSubpragToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.regimSubpragToolStripMenuItem.Text = "Regim Sub-prag";
            this.regimSubpragToolStripMenuItem.Click += new System.EventHandler(this.regimSubpragToolStripMenuItem_Click);
            // 
            // regimSaturatieToolStripMenuItem
            // 
            this.regimSaturatieToolStripMenuItem.Name = "regimSaturatieToolStripMenuItem";
            this.regimSaturatieToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.regimSaturatieToolStripMenuItem.Text = "Regim Saturatie";
            this.regimSaturatieToolStripMenuItem.Click += new System.EventHandler(this.regimSaturatieToolStripMenuItem_Click);
            // 
            // regimLiniarToolStripMenuItem
            // 
            this.regimLiniarToolStripMenuItem.Name = "regimLiniarToolStripMenuItem";
            this.regimLiniarToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.regimLiniarToolStripMenuItem.Text = "Regim Liniar";
            this.regimLiniarToolStripMenuItem.Click += new System.EventHandler(this.regimLiniarToolStripMenuItem_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.pictureBox15);
            this.tabPage2.Controls.Add(this.pictureBox13);
            this.tabPage2.Controls.Add(this.pictureBox14);
            this.tabPage2.Controls.Add(this.pictureBox12);
            this.tabPage2.Controls.Add(this.pictureBox11);
            this.tabPage2.Controls.Add(this.menuStrip3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(875, 488);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Modelare";
            // 
            // menuStrip3
            // 
            this.menuStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modeleDeSemnalMicToolStripMenuItem});
            this.menuStrip3.Location = new System.Drawing.Point(21, 20);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.Size = new System.Drawing.Size(147, 24);
            this.menuStrip3.TabIndex = 0;
            this.menuStrip3.Text = "menuStrip3";
            // 
            // modeleDeSemnalMicToolStripMenuItem
            // 
            this.modeleDeSemnalMicToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.modelSimplificatDeSemnalMicJoasaFrecventaToolStripMenuItem,
            this.modelDeSemnalMicSiInaltaFrecventaToolStripMenuItem});
            this.modeleDeSemnalMicToolStripMenuItem.Name = "modeleDeSemnalMicToolStripMenuItem";
            this.modeleDeSemnalMicToolStripMenuItem.Size = new System.Drawing.Size(139, 20);
            this.modeleDeSemnalMicToolStripMenuItem.Text = "Modele de semnal mic";
            // 
            // modelSimplificatDeSemnalMicJoasaFrecventaToolStripMenuItem
            // 
            this.modelSimplificatDeSemnalMicJoasaFrecventaToolStripMenuItem.Name = "modelSimplificatDeSemnalMicJoasaFrecventaToolStripMenuItem";
            this.modelSimplificatDeSemnalMicJoasaFrecventaToolStripMenuItem.Size = new System.Drawing.Size(328, 22);
            this.modelSimplificatDeSemnalMicJoasaFrecventaToolStripMenuItem.Text = "Model simplificat de semnal mic joasa frecventa";
            this.modelSimplificatDeSemnalMicJoasaFrecventaToolStripMenuItem.Click += new System.EventHandler(this.modelSimplificatDeSemnalMicJoasaFrecventaToolStripMenuItem_Click);
            // 
            // modelDeSemnalMicSiInaltaFrecventaToolStripMenuItem
            // 
            this.modelDeSemnalMicSiInaltaFrecventaToolStripMenuItem.Name = "modelDeSemnalMicSiInaltaFrecventaToolStripMenuItem";
            this.modelDeSemnalMicSiInaltaFrecventaToolStripMenuItem.Size = new System.Drawing.Size(328, 22);
            this.modelDeSemnalMicSiInaltaFrecventaToolStripMenuItem.Text = "Model de semnal mic si inalta frecventa";
            this.modelDeSemnalMicSiInaltaFrecventaToolStripMenuItem.Click += new System.EventHandler(this.modelDeSemnalMicSiInaltaFrecventaToolStripMenuItem_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.White;
            this.tabPage3.Controls.Add(this.pictureBox19);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.btnSimulation);
            this.tabPage3.Controls.Add(this.tbrDS);
            this.tabPage3.Controls.Add(this.lblUMrDS);
            this.tabPage3.Controls.Add(this.lblRds);
            this.tabPage3.Controls.Add(this.lblUMgds);
            this.tabPage3.Controls.Add(this.lblUMVdsat);
            this.tabPage3.Controls.Add(this.lblgmUM);
            this.tabPage3.Controls.Add(this.tbgm);
            this.tabPage3.Controls.Add(this.tbVdsat);
            this.tabPage3.Controls.Add(this.tbgds);
            this.tabPage3.Controls.Add(this.lblgds);
            this.tabPage3.Controls.Add(this.lblVdsat);
            this.tabPage3.Controls.Add(this.lblgm);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.lblObiectiv);
            this.tabPage3.Controls.Add(this.lblIdUM);
            this.tabPage3.Controls.Add(this.lblLUM);
            this.tabPage3.Controls.Add(this.lblWUM);
            this.tabPage3.Controls.Add(this.tbId);
            this.tabPage3.Controls.Add(this.tbL);
            this.tabPage3.Controls.Add(this.tbW);
            this.tabPage3.Controls.Add(this.lblId);
            this.tabPage3.Controls.Add(this.lblL);
            this.tabPage3.Controls.Add(this.lblW);
            this.tabPage3.Controls.Add(this.pictureBox18);
            this.tabPage3.Controls.Add(this.pictureBox17);
            this.tabPage3.Controls.Add(this.pictureBox16);
            this.tabPage3.Controls.Add(this.pictureBox20);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(875, 488);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Dimensionare";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(496, 457);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(232, 13);
            this.label13.TabIndex = 98;
            this.label13.Text = "gm*=491.8μS; Vdsat*=108.2mV; gds *=18.27μS";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(496, 434);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(200, 13);
            this.label12.TabIndex = 97;
            this.label12.Text = "Rezultate simulare pt noua dimensionare:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(496, 410);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(148, 13);
            this.label11.TabIndex = 96;
            this.label11.Text = "W*=2μm; L*=150nm; I =50μA;";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(496, 386);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(210, 13);
            this.label10.TabIndex = 95;
            this.label10.Text = "Redimensionarea tranzistorilor folosind a & b:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(624, 362);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 13);
            this.label9.TabIndex = 94;
            this.label9.Text = "a=2; b=1/2;";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(496, 362);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 13);
            this.label8.TabIndex = 93;
            this.label8.Text = "rds -> rds *= 1 / 2 rds";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(496, 339);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(143, 13);
            this.label7.TabIndex = 92;
            this.label7.Text = "Vdsat -> Vdsat*= Vdsat = OK";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(496, 317);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 91;
            this.label6.Text = "gm -> gm*= 2*gm";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(495, 292);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(192, 13);
            this.label5.TabIndex = 90;
            this.label5.Text = "Determinarea factorilor de ajustare a, b:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(541, 269);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(203, 13);
            this.label4.TabIndex = 89;
            this.label4.Text = "gm=247μS; Vdsat=110mV; gDS =9.23μS;";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(496, 250);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 88;
            this.label3.Text = "Rezultatele simularii PSF:";
            // 
            // btnSimulation
            // 
            this.btnSimulation.Enabled = false;
            this.btnSimulation.Location = new System.Drawing.Point(63, 442);
            this.btnSimulation.Name = "btnSimulation";
            this.btnSimulation.Size = new System.Drawing.Size(75, 23);
            this.btnSimulation.TabIndex = 87;
            this.btnSimulation.Text = "Simuleaza";
            this.btnSimulation.UseVisualStyleBackColor = true;
            this.btnSimulation.Click += new System.EventHandler(this.btnSimulation_Click);
            // 
            // tbrDS
            // 
            this.tbrDS.Location = new System.Drawing.Point(264, 435);
            this.tbrDS.Name = "tbrDS";
            this.tbrDS.Size = new System.Drawing.Size(100, 20);
            this.tbrDS.TabIndex = 86;
            // 
            // lblUMrDS
            // 
            this.lblUMrDS.AutoSize = true;
            this.lblUMrDS.Location = new System.Drawing.Point(369, 442);
            this.lblUMrDS.Name = "lblUMrDS";
            this.lblUMrDS.Size = new System.Drawing.Size(22, 13);
            this.lblUMrDS.TabIndex = 85;
            this.lblUMrDS.Text = "kΩ";
            // 
            // lblRds
            // 
            this.lblRds.AutoSize = true;
            this.lblRds.Location = new System.Drawing.Point(218, 442);
            this.lblRds.Name = "lblRds";
            this.lblRds.Size = new System.Drawing.Size(37, 13);
            this.lblRds.TabIndex = 84;
            this.lblRds.Text = "rDS = ";
            // 
            // lblUMgds
            // 
            this.lblUMgds.AutoSize = true;
            this.lblUMgds.Location = new System.Drawing.Point(369, 412);
            this.lblUMgds.Name = "lblUMgds";
            this.lblUMgds.Size = new System.Drawing.Size(20, 13);
            this.lblUMgds.TabIndex = 83;
            this.lblUMgds.Text = "μS";
            // 
            // lblUMVdsat
            // 
            this.lblUMVdsat.AutoSize = true;
            this.lblUMVdsat.Location = new System.Drawing.Point(369, 390);
            this.lblUMVdsat.Name = "lblUMVdsat";
            this.lblUMVdsat.Size = new System.Drawing.Size(22, 13);
            this.lblUMVdsat.TabIndex = 82;
            this.lblUMVdsat.Text = "mV";
            // 
            // lblgmUM
            // 
            this.lblgmUM.AutoSize = true;
            this.lblgmUM.Location = new System.Drawing.Point(369, 363);
            this.lblgmUM.Name = "lblgmUM";
            this.lblgmUM.Size = new System.Drawing.Size(20, 13);
            this.lblgmUM.TabIndex = 81;
            this.lblgmUM.Text = "μS";
            // 
            // tbgm
            // 
            this.tbgm.Location = new System.Drawing.Point(264, 363);
            this.tbgm.Name = "tbgm";
            this.tbgm.Size = new System.Drawing.Size(100, 20);
            this.tbgm.TabIndex = 70;
            // 
            // tbVdsat
            // 
            this.tbVdsat.Location = new System.Drawing.Point(264, 386);
            this.tbVdsat.Name = "tbVdsat";
            this.tbVdsat.Size = new System.Drawing.Size(100, 20);
            this.tbVdsat.TabIndex = 71;
            // 
            // tbgds
            // 
            this.tbgds.Location = new System.Drawing.Point(264, 409);
            this.tbgds.Name = "tbgds";
            this.tbgds.Size = new System.Drawing.Size(100, 20);
            this.tbgds.TabIndex = 72;
            // 
            // lblgds
            // 
            this.lblgds.AutoSize = true;
            this.lblgds.Location = new System.Drawing.Point(222, 412);
            this.lblgds.Name = "lblgds";
            this.lblgds.Size = new System.Drawing.Size(36, 13);
            this.lblgds.TabIndex = 80;
            this.lblgds.Text = "gds = ";
            // 
            // lblVdsat
            // 
            this.lblVdsat.AutoSize = true;
            this.lblVdsat.Location = new System.Drawing.Point(212, 386);
            this.lblVdsat.Name = "lblVdsat";
            this.lblVdsat.Size = new System.Drawing.Size(46, 13);
            this.lblVdsat.TabIndex = 79;
            this.lblVdsat.Text = "Vdsat = ";
            // 
            // lblgm
            // 
            this.lblgm.AutoSize = true;
            this.lblgm.Location = new System.Drawing.Point(222, 360);
            this.lblgm.Name = "lblgm";
            this.lblgm.Size = new System.Drawing.Size(33, 13);
            this.lblgm.TabIndex = 78;
            this.lblgm.Text = "gm = ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(541, 228);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 13);
            this.label2.TabIndex = 77;
            this.label2.Text = "W=1μm; L=150nm; Id =25μA;";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(496, 205);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(373, 13);
            this.label1.TabIndex = 76;
            this.label1.Text = "Se initializeaza W, L, Id: prima estimare a parametrilor tranzistorului de referi" +
    "nta";
            // 
            // lblObiectiv
            // 
            this.lblObiectiv.AutoSize = true;
            this.lblObiectiv.Location = new System.Drawing.Point(496, 177);
            this.lblObiectiv.Name = "lblObiectiv";
            this.lblObiectiv.Size = new System.Drawing.Size(238, 13);
            this.lblObiectiv.TabIndex = 75;
            this.lblObiectiv.Text = "Obiectiv: gm =500μS; Vdsat =110mV; gds =20μS";
            // 
            // lblIdUM
            // 
            this.lblIdUM.AutoSize = true;
            this.lblIdUM.Location = new System.Drawing.Point(170, 412);
            this.lblIdUM.Name = "lblIdUM";
            this.lblIdUM.Size = new System.Drawing.Size(20, 13);
            this.lblIdUM.TabIndex = 74;
            this.lblIdUM.Text = "μA";
            // 
            // lblLUM
            // 
            this.lblLUM.AutoSize = true;
            this.lblLUM.Location = new System.Drawing.Point(169, 386);
            this.lblLUM.Name = "lblLUM";
            this.lblLUM.Size = new System.Drawing.Size(21, 13);
            this.lblLUM.TabIndex = 73;
            this.lblLUM.Text = "nm";
            // 
            // lblWUM
            // 
            this.lblWUM.AutoSize = true;
            this.lblWUM.Location = new System.Drawing.Point(169, 360);
            this.lblWUM.Name = "lblWUM";
            this.lblWUM.Size = new System.Drawing.Size(21, 13);
            this.lblWUM.TabIndex = 72;
            this.lblWUM.Text = "μm";
            // 
            // tbId
            // 
            this.tbId.Location = new System.Drawing.Point(63, 409);
            this.tbId.Name = "tbId";
            this.tbId.Size = new System.Drawing.Size(100, 20);
            this.tbId.TabIndex = 71;
            // 
            // tbL
            // 
            this.tbL.Location = new System.Drawing.Point(63, 383);
            this.tbL.Name = "tbL";
            this.tbL.Size = new System.Drawing.Size(100, 20);
            this.tbL.TabIndex = 70;
            // 
            // tbW
            // 
            this.tbW.Location = new System.Drawing.Point(63, 357);
            this.tbW.Name = "tbW";
            this.tbW.Size = new System.Drawing.Size(100, 20);
            this.tbW.TabIndex = 69;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(36, 412);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(28, 13);
            this.lblId.TabIndex = 68;
            this.lblId.Text = "Id = ";
            // 
            // lblL
            // 
            this.lblL.AutoSize = true;
            this.lblL.Location = new System.Drawing.Point(36, 386);
            this.lblL.Name = "lblL";
            this.lblL.Size = new System.Drawing.Size(25, 13);
            this.lblL.TabIndex = 67;
            this.lblL.Text = "L = ";
            // 
            // lblW
            // 
            this.lblW.AutoSize = true;
            this.lblW.Location = new System.Drawing.Point(36, 360);
            this.lblW.Name = "lblW";
            this.lblW.Size = new System.Drawing.Size(30, 13);
            this.lblW.TabIndex = 66;
            this.lblW.Text = "W = ";
            // 
            // PictureLogo
            // 
            this.PictureLogo.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.rsz_vlogo;
            this.PictureLogo.Location = new System.Drawing.Point(750, 362);
            this.PictureLogo.Name = "PictureLogo";
            this.PictureLogo.Size = new System.Drawing.Size(125, 126);
            this.PictureLogo.TabIndex = 63;
            this.PictureLogo.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.Caractdetransfer;
            this.pictureBox10.Location = new System.Drawing.Point(416, 29);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(463, 263);
            this.pictureBox10.TabIndex = 61;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.MouseHover += new System.EventHandler(this.pictureBox10_MouseHover);
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.caractdeiesire;
            this.pictureBox9.Location = new System.Drawing.Point(416, 29);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(463, 263);
            this.pictureBox9.TabIndex = 60;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.MouseHover += new System.EventHandler(this.pictureBox9_MouseHover);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.functliniar;
            this.pictureBox8.Location = new System.Drawing.Point(6, 298);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(330, 186);
            this.pictureBox8.TabIndex = 59;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.MouseHover += new System.EventHandler(this.pictureBox8_MouseHover);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.condliniar;
            this.pictureBox7.Location = new System.Drawing.Point(6, 193);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(245, 86);
            this.pictureBox7.TabIndex = 58;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.MouseHover += new System.EventHandler(this.pictureBox7_MouseHover);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.functsat;
            this.pictureBox6.Location = new System.Drawing.Point(6, 298);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(558, 186);
            this.pictureBox6.TabIndex = 56;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.MouseHover += new System.EventHandler(this.pictureBox6_MouseHover);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.condsat;
            this.pictureBox5.Location = new System.Drawing.Point(6, 193);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(240, 83);
            this.pictureBox5.TabIndex = 55;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.MouseHover += new System.EventHandler(this.pictureBox5_MouseHover);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.functsubprag;
            this.pictureBox4.Location = new System.Drawing.Point(6, 298);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(245, 74);
            this.pictureBox4.TabIndex = 54;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseHover += new System.EventHandler(this.pictureBox4_MouseHover);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.functblocat;
            this.pictureBox2.Location = new System.Drawing.Point(6, 298);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(94, 42);
            this.pictureBox2.TabIndex = 52;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseHover += new System.EventHandler(this.pictureBox2_MouseHover);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.condsubprag;
            this.pictureBox3.Location = new System.Drawing.Point(6, 193);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(152, 50);
            this.pictureBox3.TabIndex = 53;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.condblocat;
            this.pictureBox1.Location = new System.Drawing.Point(6, 196);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(143, 48);
            this.pictureBox1.TabIndex = 51;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // pictureBoxSimbol
            // 
            this.pictureBoxSimbol.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxSimbol.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.CMOS_simbol;
            this.pictureBoxSimbol.Location = new System.Drawing.Point(-5, 2);
            this.pictureBoxSimbol.Name = "pictureBoxSimbol";
            this.pictureBoxSimbol.Size = new System.Drawing.Size(286, 148);
            this.pictureBoxSimbol.TabIndex = 40;
            this.pictureBoxSimbol.TabStop = false;
            this.pictureBoxSimbol.MouseHover += new System.EventHandler(this.pictureBoxSimbol_MouseHover);
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.dependentatransconductanta;
            this.pictureBox15.Location = new System.Drawing.Point(407, 2);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(467, 236);
            this.pictureBox15.TabIndex = 66;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.MouseHover += new System.EventHandler(this.pictureBox15_MouseHover);
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.rsz_vlogo;
            this.pictureBox13.Location = new System.Drawing.Point(750, 362);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(125, 126);
            this.pictureBox13.TabIndex = 64;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.schemasemnalmicinaltafrecv;
            this.pictureBox14.Location = new System.Drawing.Point(0, 57);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(401, 138);
            this.pictureBox14.TabIndex = 65;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.MouseHover += new System.EventHandler(this.pictureBox14_MouseHover);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.transconductanta_semnalmic2;
            this.pictureBox12.Location = new System.Drawing.Point(3, 244);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(684, 233);
            this.pictureBox12.TabIndex = 2;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.MouseHover += new System.EventHandler(this.pictureBox12_MouseHover);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.modelsimplificatsemnalmic;
            this.pictureBox11.Location = new System.Drawing.Point(21, 68);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(263, 127);
            this.pictureBox11.TabIndex = 1;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.MouseHover += new System.EventHandler(this.pictureBox11_MouseHover);
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.ilustraregradlibertate;
            this.pictureBox19.Location = new System.Drawing.Point(22, 177);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(369, 174);
            this.pictureBox19.TabIndex = 99;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.MouseLeave += new System.EventHandler(this.pictureBox19_MouseLeave);
            this.pictureBox19.MouseHover += new System.EventHandler(this.pictureBox19_MouseHover);
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.rsz_vlogo;
            this.pictureBox18.Location = new System.Drawing.Point(363, 19);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(125, 126);
            this.pictureBox18.TabIndex = 65;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.MouseHover += new System.EventHandler(this.pictureBox18_MouseHover);
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.ec_generale_redimensionare_gm;
            this.pictureBox17.Location = new System.Drawing.Point(544, 7);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(328, 152);
            this.pictureBox17.TabIndex = 1;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.ec_generale_redimensionare;
            this.pictureBox16.Location = new System.Drawing.Point(6, 6);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(299, 152);
            this.pictureBox16.TabIndex = 0;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.ec_ucox;
            this.pictureBox20.Location = new System.Drawing.Point(117, 3);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(665, 156);
            this.pictureBox20.TabIndex = 100;
            this.pictureBox20.TabStop = false;
            // 
            // GeneralitatiTMOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(882, 512);
            this.Controls.Add(this.tabControlFunctionare);
            this.MainMenuStrip = this.menuStrip3;
            this.Name = "GeneralitatiTMOS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TMOS-Functionare, Modelare si Dimensionare";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.GeneralitatiTMOS_FormClosed);
            this.tabControlFunctionare.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSimbol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlFunctionare;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label Bulck;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label G;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem caracteristiciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem caracteristicaDeTransferToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem caracteristicaDeIesireToolStripMenuItem;
        private System.Windows.Forms.Label Simbol;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label Drena;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label D;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label Sursa;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label Grila;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label FunctionareB;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label S;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label B;
        private System.Windows.Forms.PictureBox pictureBoxSimbol;
        private System.Windows.Forms.Label CondB;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem regimuriDeFunctionareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regimBlocatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regimSubpragToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regimSaturatieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem regimLiniarToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PictureBox PictureLogo;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem modeleDeSemnalMicToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modelSimplificatDeSemnalMicJoasaFrecventaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modelDeSemnalMicSiInaltaFrecventaToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.Label lblWUM;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.TextBox tbL;
        private System.Windows.Forms.TextBox tbW;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label lblL;
        private System.Windows.Forms.Label lblW;
        private System.Windows.Forms.Label lblIdUM;
        private System.Windows.Forms.Label lblLUM;
        private System.Windows.Forms.Label lblgmUM;
        private System.Windows.Forms.TextBox tbgm;
        private System.Windows.Forms.TextBox tbVdsat;
        private System.Windows.Forms.TextBox tbgds;
        private System.Windows.Forms.Label lblgds;
        private System.Windows.Forms.Label lblVdsat;
        private System.Windows.Forms.Label lblgm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblObiectiv;
        private System.Windows.Forms.Label lblUMgds;
        private System.Windows.Forms.Label lblUMVdsat;
        private System.Windows.Forms.Label lblUMrDS;
        private System.Windows.Forms.Label lblRds;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSimulation;
        private System.Windows.Forms.TextBox tbrDS;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;



    }
}