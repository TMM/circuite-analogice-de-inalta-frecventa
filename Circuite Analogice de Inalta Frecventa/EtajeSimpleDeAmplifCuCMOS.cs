﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Circuite_Analogice_de_Inalta_Frecventa
{
    public partial class EtajeSimpleDeAmplifCuCMOS : Form
    {
        public EtajeSimpleDeAmplifCuCMOS()
        {
            InitializeComponent();
            HideAll();
            HideTab4();
            this.MaximizeBox = false;
            this.MaximumSize = this.MinimumSize = this.Size;
        }

        private void HideAll()
        {
            pictureBox1.Hide();
            pictureBox2.Hide();
            pictureBox3.Hide();
            pictureBox4.Hide();
            pictureBox5.Hide();
            pictureBox6.Hide();
            pictureBox7.Hide();
            pictureBox8.Hide();
            pictureBox9.Hide();
            pictureBox10.Hide();
            pictureBox11.Hide();
        }

        private void HideTab4()
        {
            lblVSS.Hide();
            lblVDD.Hide();
            lblVin_pk.Hide();
            lblCL.Hide();
            lblBandaFrecv.Hide();
            lblCerinta.Hide();
            label8.Hide();
            label7.Hide();
            label6.Hide();
            label5.Hide();
            label4.Hide();
            label3.Hide();
            label2.Hide();
            lblVdsat.Hide();
            label1.Hide();
            label15.Hide();
            label14.Hide();
            label13.Hide();
            label12.Hide();
            label11.Hide();
            label10.Hide();
            label9.Hide();
            label17.Hide();
            label16.Hide();
            label20.Hide();
            label19.Hide();
            label18.Hide();
            tbIdrenN.Hide();
            tbIdrenaP.Hide();
            btnSimulare.Hide();
            tbIdPMOS.Hide();
            tbWpeLNMOS.Hide();
            tbWpeLPMOS.Hide();
            tbVdsatNMOS.Hide();
            tbVdsatPMOS.Hide();
            tbIdNMOS.Hide();
            label30.Hide();
            label29.Hide();
            label28.Hide();
            label25.Hide();
            label26.Hide();
            label27.Hide();
            label24.Hide();
            tbVdsatN.Hide();
            tbVdsatP.Hide();
            tbVthN.Hide();
            tbVthP.Hide();
            label23.Hide();
            label22.Hide();
            tbWpeLP.Hide();
            tbWpeLN.Hide();
            label21.Hide();
            PictureLogo.Hide();
            pictureBox18.Hide();
            pictureBox19.Hide();
        }

        private void EtajeSimpleDeAmplifCuCMOS_FormClosed(object sender, FormClosedEventArgs e)
        {
            mainForm mfrm = new mainForm();
            this.Visible = false;
            mfrm.ShowDialog();
            this.Close();
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            ToolTip Generalitati = new ToolTip();
            Generalitati.IsBalloon = true;
            Generalitati.AutoPopDelay = 30000;
            Generalitati.SetToolTip(pictureBox1, "Pentru ca amplificatorul sa functioneze(sa amplifice un semnal de valoare mica) este necesara\nalegerea optima a punctului static de functionare (in curent continuu) dupa cum se observa si in figura alaturata\nastfel incat sa se asigure o functionare corecta a tranzistorului MOS. Acesta trebuie sa aiba asemnea\ncoordonate in sistemul (vout, Id) astfel incat la valori de varf ale semnalului tranzistorul sa ramana in\nregim saturat.");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            ToolTip Info = new ToolTip();
            Info.IsBalloon = true;
            Info.AutoPopDelay = 30000;
            Info.SetToolTip(pictureBox2, "Din punctul de vedere al electronicii, notiunea de amplificator este data unui dispozitiv format din\nelemente active care poate primi la intrare un semnal (tensiune sau curent) si in urma prelucrarii\nfurnizeaza la bornele de iesire un alt semnal (tensiune sau curent) cu valorile caracteristice marite \n(de exemplu amplitudinea), cu ajutorul energiei luate de la sursa de alimentare.\n\nIn cazul amplificatoarelor simple cu tranzistori MOS semnalul de amplificat este o tensiune\naplicata intre grila si sursa ce este convertita in curent prin transconductanta gm( Id= gm * Vds) si apoi\nculeasa ca tensiune la iesire de pe o impedanta de sarcina.");
        }

        private void semnalMareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideAll();
            pictureBox1.Show();
            pictureBox2.Show();
            pictureBox3.Show();
            pictureBox4.Show();
            pictureBox5.Show();
        }

        private void semnalMicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideAll();
            pictureBox6.Show();
            pictureBox7.Show();
            pictureBox8.Show();
            pictureBox9.Show();
            pictureBox10.Show();
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            ToolTip Generalitati = new ToolTip();
            Generalitati.IsBalloon = true;
            Generalitati.AutoPopDelay = 30000;
            Generalitati.SetToolTip(pictureBox1, "Datorita capacitatilor parazite ale circuitului, apar doua singularitati: un zerou pozitiv datorat capacitatii\nCGD si un pol_nedominant datorat capacitatii de sarcina CL");
        }

        private void pictureBox9_MouseLeave(object sender, EventArgs e)
        {
            pictureBox11.Hide();
            pictureBox6.Show();
            pictureBox7.Show();
            pictureBox8.Show();
            pictureBox9.Show();
            pictureBox10.Show();
        }

        private void pictureBox9_MouseEnter(object sender, EventArgs e)
        {
            pictureBox6.Hide();
            pictureBox7.Hide();
            pictureBox8.Hide();
            pictureBox9.Show();
            pictureBox10.Hide();
            pictureBox11.Show();
        }

        private void pictureBox12_MouseHover(object sender, EventArgs e)
        {
            ToolTip Generalitati = new ToolTip();
            Generalitati.IsBalloon = true;
            Generalitati.AutoPopDelay = 30000;
            Generalitati.SetToolTip(pictureBox12, "In tab-ul din stanga s-a aratat faptul ca, capacitatea CGD de pe calea de semnal a amplificatorului\nsimplu cu sarcina rezistiva introduce o bucla de reactie ce face ca produsul amplificare banda-GBW sa\nramana constant. Intrucat aceasta capacitate nu poate fi micsorata prea mult, este necesara ruperea buclei\nde reactie. O metoda prin care se poate realiza acest fapt este prin cascodare (introducerea unui\ntranzistor suplimentar intre nodurile unde se situeaza aceasta capacitate: Out si in Grila tranzistorului\namplificator).");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            ToolTip Generalitati = new ToolTip();
            Generalitati.IsBalloon = true;
            Generalitati.AutoPopDelay = 30000;
            Generalitati.SetToolTip(pictureBox4, "Se observa ca domeniul Tensiunii de iesire este intre Vdsat si VDD\n Vout ε [Vdsat, VDD]");
        }

        private void pictureBox13_MouseHover(object sender, EventArgs e)
        {
            ToolTip Generalitati = new ToolTip();
            Generalitati.IsBalloon = true;
            Generalitati.AutoPopDelay = 30000;
            Generalitati.SetToolTip(pictureBox13, "Se observa ca domeniul Tensiunii de iesire este intre Vdsat1+Vdsat2 si VDD\n Vout ε [Vdsat1+Vdsat2, VDD]");
        }

        private void pictureBox14_MouseHover(object sender, EventArgs e)
        {
            ToolTip Generalitati = new ToolTip();
            Generalitati.IsBalloon = true;
            Generalitati.AutoPopDelay = 30000;
            Generalitati.SetToolTip(pictureBox14, "Formulele amplificarii, a polului dominant, si a frecventei unde castigul este unitar(GBW).\nDin analiza circuitului se observa ca mai mai apare un pol parazit in nodul 1(fpol_[1]).\nDar datorita valorilor mici a capacitatilor parazite, cat si a impedantei echivalente mici in acel nod,\nacesta se afla la frecvente inalte si nu afecteaza functionarea in banda a amplificatorului.");
        }

        private void pictureBox15_MouseHover(object sender, EventArgs e)
        {
            ToolTip Generalitati = new ToolTip();
            Generalitati.IsBalloon = true;
            Generalitati.AutoPopDelay = 30000;
            Generalitati.SetToolTip(pictureBox15, "Proiectarea acestui amplificator este diferita de cele prezentate pana acum datorita faptului ca odata cu\nimplementarea cascodelor in circuit, amplificarea acestuia creste de la maxim 20 dB, cat am obtinut pentru\namplificatoarele anterioare, pana la peste 60 dB. In functie de specificatiile impuse, precum GBW, SR si\nCL se pot diomensiona toti tranzistorii circuitului.");
        }

        private void btnSimulare_Click(object sender, EventArgs e)
        {
            float IdrenaN0, IdrenaP0, IdrenaNMOS, IdrenaPMOS, WLN0, WLP0, WLNMOS, WLPMOS, VdsatN0, VdsatP0, VdsatNMOS, VdsatPMOS, VthN0, VthP0;
            IdrenaN0 = float.Parse(tbIdrenN.Text);
            IdrenaP0 = float.Parse(tbIdrenaP.Text);
            IdrenaNMOS = float.Parse(tbIdNMOS.Text);
            IdrenaPMOS = float.Parse(tbIdPMOS.Text);
            WLN0 = float.Parse(tbWpeLN.Text);
            WLP0 = float.Parse(tbWpeLP.Text);
            WLNMOS = float.Parse(tbWpeLNMOS.Text);
            WLPMOS = float.Parse(tbWpeLPMOS.Text);
            VdsatN0 = float.Parse(tbVdsatN.Text);
            VdsatP0 = float.Parse(tbVdsatP.Text);
            VdsatNMOS = float.Parse(tbVdsatNMOS.Text);
            VdsatPMOS = float.Parse(tbVdsatPMOS.Text);
            VthN0 = float.Parse(tbVthN.Text);
            VthP0 = float.Parse(tbVthP.Text);

            if (VdsatNMOS != 0 && VdsatPMOS != 0)
            {
                WLNMOS = (IdrenaNMOS * WLN0 * VdsatN0 * VdsatN0) / (IdrenaN0 * VdsatNMOS * VdsatNMOS);
                WLPMOS = (IdrenaPMOS * WLP0 * VdsatP0 * VdsatP0) / (IdrenaP0 * VdsatPMOS * VdsatPMOS);
                // Vdsat = Vgs - Vth
                VthN0 = 600 - VdsatN0;
                VthP0= 600 - VdsatP0;

                tbWpeLNMOS.Text = WLNMOS.ToString();
                tbWpeLPMOS.Text = WLPMOS.ToString();
                
                tbVthN.Text = VthN0.ToString();
                tbVthP.Text = VthP0.ToString();
            }
            else
                MessageBox.Show("Introduceti Valori pentru Vdsat pentru a putea simula", "Atentie!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void amplificatorSursaComunaCuSarcinaRezistivaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lblVSS.Show();
            lblVDD.Show();
            lblVin_pk.Show();
            lblCL.Show();
            lblBandaFrecv.Show();
            lblCerinta.Show();
            label8.Show();
            label7.Show();
            label6.Show();
            label5.Show();
            label4.Show();
            label3.Show();
            label2.Show();
            lblVdsat.Show();
            label1.Show();
            label15.Show();
            label14.Show();
            label13.Show();
            label12.Show();
            label11.Show();
            label10.Show();
            label9.Show();
            label17.Show();
            label16.Show();
            label20.Show();
            label19.Show();
            label18.Show();
            tbIdrenN.Show();
            tbIdrenaP.Show();
            btnSimulare.Show();
            tbIdPMOS.Show();
            tbWpeLNMOS.Show();
            tbWpeLPMOS.Show();
            tbVdsatNMOS.Show();
            tbVdsatPMOS.Show();
            tbIdNMOS.Show();
            label30.Show();
            label29.Show();
            label28.Show();
            label25.Show();
            label26.Show();
            label27.Show();
            label24.Show();
            tbVdsatN.Show();
            tbVdsatP.Show();
            tbVthN.Show();
            tbVthP.Show();
            label23.Show();
            label22.Show();
            tbWpeLP.Show();
            tbWpeLN.Show();
            label21.Show();
            pictureBox18.Show();
            pictureBox19.Show();
            // Chestii ce se modifica :D
            label3.Text = "vout_max = (VDD-Vdsat) / 2";
            label5.Text = "VOUT_max=0.9V       Av=9";
            label8.Text = "SR > 2pi * BW * Vout (SR > 565.2V/μs)";
            label9.Text = "Alegem SR = 600V/us";
            label10.Text = "Id = 3mA";
            label12.Text = "Av = -gm*RL(cand RL<<rDS) = -(2IdRL)/Vdsat";
            label14.Text = "RL = 300Ω";
        }

        private void amplificatorulCascodaCuSarcinaRezistivaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label3.Text = "vout_max = (VDD-Vdsat1-Vdsat2) / 2";
            label5.Text = "VOUT_max=0.8V       Av=8";
            label8.Text = "SR > 2pi * BW * Vout (SR > 499.5V/μs)";
            label9.Text = "Alegem SR = 500V/us";
            label10.Text = "Id = 2.5mA";
            label12.Text = "Av = gm*RL(cand RL<<rDS)=(2IdRL)/Vdsat";
            label14.Text = "RL = 320Ω";
        }
    }
}
