﻿namespace Circuite_Analogice_de_Inalta_Frecventa
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.curs1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.curs1ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.curs2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.curs1ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(882, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(12, 20);
            // 
            // curs1ToolStripMenuItem
            // 
            this.curs1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.curs1ToolStripMenuItem1,
            this.curs2ToolStripMenuItem});
            this.curs1ToolStripMenuItem.Name = "curs1ToolStripMenuItem";
            this.curs1ToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.curs1ToolStripMenuItem.Text = "Cursuri";
            // 
            // curs1ToolStripMenuItem1
            // 
            this.curs1ToolStripMenuItem1.Name = "curs1ToolStripMenuItem1";
            this.curs1ToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.curs1ToolStripMenuItem1.Text = "Curs 1";
            this.curs1ToolStripMenuItem1.Click += new System.EventHandler(this.curs1ToolStripMenuItem1_Click);
            // 
            // curs2ToolStripMenuItem
            // 
            this.curs2ToolStripMenuItem.Name = "curs2ToolStripMenuItem";
            this.curs2ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.curs2ToolStripMenuItem.Text = "Curs 2";
            this.curs2ToolStripMenuItem.Click += new System.EventHandler(this.curs2ToolStripMenuItem_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::Circuite_Analogice_de_Inalta_Frecventa.Properties.Resources.logo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(882, 512);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Circuite Analogice de Inalta Frecventa";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem curs1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem curs1ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem curs2ToolStripMenuItem;
    }
}

