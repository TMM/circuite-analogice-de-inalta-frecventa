﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Circuite_Analogice_de_Inalta_Frecventa
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MaximumSize = this.MinimumSize = this.Size;
        }

        private void curs1ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            GeneralitatiTMOS gen = new GeneralitatiTMOS();
            this.Visible = false;
            gen.ShowDialog();
            this.Close();
        }

        private void curs2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EtajeSimpleDeAmplifCuCMOS et = new EtajeSimpleDeAmplifCuCMOS();
            this.Visible = false;
            et.ShowDialog();
            this.Close();
        }
    }
}
